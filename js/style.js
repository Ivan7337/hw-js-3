// 1. циклы нужны для того что бы повторять какое то действие до тех пор, пока оно не выполнит условие 

// 2. я использовал циклы что бы получить четное или не четное число, а так же, что бы сделать валидацию


// 3. Явное приведение типов данных происходит тогда, когда кодер указывает, какой тип данных должен быть использован для определенной операции.
// Неявное приведение типов данных происходит автоматически при выполнении операций.         




const userInput = prompt("Please enter a number:");
const number = parseInt(userInput);

if (!isNaN(number)) {
  const multiplesOfFive = [];
  for (let i = 0; i <= number; i++) {
    if (i % 5 === 0) {
      multiplesOfFive.push(i);
    }
  }
  if (multiplesOfFive.length > 0) {
    console.log("Multiples of 5:", multiplesOfFive);
  } else {
    console.log("Sorry, no numbers");
  }
} else {
  console.log("Invalid input");
}